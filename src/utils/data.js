import _ from "lodash";
import faker from "faker";

export const topicSource = _.times(3, () => ({
  title: faker.lorem.word(),
  number: faker.random.number({ min: 10, max: 50 }),
  date: faker.date.month()
}));

export const articleSource = [
  {
    title: "Traffic signal response for autonomous vehicles",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: true,
    id:1,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "How autonomous vehicles respond to yellow traffic signal",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:2,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Assisted driving for autonomous vehicles",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:3,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Traffic signal response for self driving vehicles",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: true,
    id:4,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Determining Hotword suitability for voice activated systems",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:5,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title:
      "Improving neural network's efficiency for limited memory environments",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:6,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Improving accuracy of neural network with adversarial images",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: true,
    id:7,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Improving electrical conductivity of a tyre",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:8,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  },
  {
    title: "Combatting heart disease and stroke with diet",
    description: faker.lorem.lines(2),
    date: faker.date.month(),
    video: false,
    id:9,
    highlights: [
      {
        key: 0,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 1,
        title:
          "A signal processing based on “slip and velocity” module of the system that evaluates the clutter ridge (the area of high peak values). The evaluation is used in combination with Fig 6 to determine the velocity and slip angle of the vehicle.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 2,
        title:
          "Training engine uses the training images which includes the original training images plus the adversarial images, to train the neural network image classifier.",
        content: faker.lorem.paragraphs()
      },
      {
        key: 3,
        title:
          "The problem that the present patent has addressed is related to the fact that velocity measurement using wheels can not be perfect, especially in slippery when is used in AVs",
        content: faker.lorem.paragraphs()
      }
    ]
  }
];
