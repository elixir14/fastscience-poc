import React from 'react'
import { Header, Segment, Image } from 'semantic-ui-react'
import logo from '../../assets/logo/logo.png'
import { NavLink } from 'react-router-dom'

const NavHeader = () => (
  <Segment clearing className="navbar">
    <NavLink to="/">
      <Header as="h2" floated="left" className="logo">
        <Image src={logo} />
      </Header>
    </NavLink>
    <div className="headerTitle">Fast Science</div>
  </Segment>
)

export default NavHeader
