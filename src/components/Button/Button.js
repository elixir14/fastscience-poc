import React from "react";
import { Button as Btn} from "semantic-ui-react";

export default function Button(props) {
  return <Btn primary>{props.title}</Btn>;
}
