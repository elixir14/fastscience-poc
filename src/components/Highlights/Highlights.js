import React from 'react'
import { Modal } from 'semantic-ui-react'
import Points from '../Points/Points'
import { useParams } from 'react-router-dom'
import { articleSource } from '../../utils/data'

const Highlights = props => {
  const { article } = useParams()
  const data = articleSource.find(({ id }) => id === parseInt(article))
  return (
    <Modal size="large" open={true} closeIcon onClose={e => props.history.goBack()}>
      <Modal.Header>Highlights from the document</Modal.Header>
      <Modal.Content>
        <Points points={data.highlights} />
      </Modal.Content>
    </Modal>
  )
}

export default Highlights
