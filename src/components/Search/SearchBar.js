import _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Search, Grid, Label, Dropdown } from "semantic-ui-react";

const resultRenderer = ({ title }) => <Label content={title} />;
resultRenderer.propTypes = {
  title: PropTypes.string
};
export default class SearchBar extends Component {
  handleSearchChange = (e, { value }) => {
    this.props.onSearch(value);
  };
  handleResultSelect = (e, { result }) => {
    this.props.onResultSelect(result);
  };
  handleFilterChange = (e, { value }) => {
    this.props.onFilter(value);
  };
  render() {
    return (
      <Grid columns={3} stackable className="search">
        <Grid.Column width={2} className="filter">
          <Label size="medium">Filter Summaries</Label>
        </Grid.Column>
        <Grid.Column floated="left">
          <Dropdown
            clearable
            onChange={this.handleFilterChange}
            options={this.props.options}
            placeholder="Grant Date"
            selection
            value={this.props.filterValue}
          />
        </Grid.Column>
        <Grid.Column width={4}>
          <Search
          size="small"
            placeholder="Search"
            loading={this.props.loading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {
              leading: true
            })}
            results={this.props.results}
            value={this.props.value}
            resultRenderer={resultRenderer}
          />
        </Grid.Column>
      </Grid>
    );
  }
}
