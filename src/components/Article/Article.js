import React from 'react'
import { Card, Image } from 'semantic-ui-react'
import rocket from '../../assets/image/rocket.jpg'
import { Button } from 'semantic-ui-react'

const Article = props => {
  const { data } = { ...props }
  return (
    <Card color="red" raised>
      <Image src={rocket} />
      <Card.Content>
        <Card.Header as="h5">{data.title}</Card.Header>
        <Card.Description>{data.description}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button size="tiny" primary onClick={e => props.history.push(`/articles/${data.id}/highlight`)}>
          HIGHLIGHTS
        </Button>
        {data.video ? (
          <Button size="tiny" primary>
            VIDEO SUMMARY
          </Button>
        ) : (
          ''
        )}
      </Card.Content>
    </Card>
  )
}

export default Article
