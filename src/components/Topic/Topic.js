import React from "react";
import { Card, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";
import rocket from "../../assets/image/rocket.jpg";

const Topic = (data) => (
  <Card color='green' raised className="topic">
    <Image src={rocket} wrapped ui={false} />
    <Card.Content>
      <Card.Header>{data.data.title}</Card.Header>
      <Link to={"/articles"}>
        <Card.Description>
          {data.data.number} patents, {data.data.date}
        </Card.Description>
      </Link>
    </Card.Content>
  </Card>
);

export default Topic;
