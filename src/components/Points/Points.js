import React, { Component } from 'react'
import { Accordion, Icon, Image, Grid } from 'semantic-ui-react'
import flow from '../../assets/image/flow.png'

export default class Points extends Component {
  state = { activeIndex: -1 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { activeIndex } = this.state
    return (
      <Accordion fluid>
        {this.props.points.map((data, index) => {
          return (
            <React.Fragment key={index}>
              <Accordion.Title active={activeIndex === index} index={index} onClick={this.handleClick}>
                <Icon name="dropdown" />
                {data.title}
              </Accordion.Title>
              <Accordion.Content active={activeIndex === index}>
                <Grid stackable columns={2}>
                  <Grid.Column width={4}>
                    <Image src={flow} size="medium" />
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <p className="highlight-content">{data.content}</p>
                  </Grid.Column>
                </Grid>
              </Accordion.Content>
            </React.Fragment>
          )
        })}
      </Accordion>
    )
  }
}
