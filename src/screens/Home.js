import React, { Component } from 'react'
import { Grid, Container } from 'semantic-ui-react'
import Topic from '../components/Topic/Topic'
import { topicSource } from '../utils/data'

const initialState = { topics: topicSource }

class Home extends Component {
  state = initialState
  render() {
    return (
      <Container className="topic">
        <Grid columns="three">
          {this.state.topics.map((data, index) => {
            return (
              <Grid.Column key={index}>
                <Topic data={data} />
              </Grid.Column>
            )
          })}
        </Grid>
      </Container>
    )
  }
}

export default Home
