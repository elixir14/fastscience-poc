import _ from 'lodash'
import React, { Component } from 'react'
import { Grid, Container } from 'semantic-ui-react'
import Article from '../components/Article/Article'
import SearchBar from '../components/Search/SearchBar'
import { articleSource } from '../utils/data'

const initialState = {
  articles: articleSource,
  isLoading: false,
  results: [],
  value: '',
  filterValue: '',
  options: [],
}

class Articles extends Component {
  state = initialState

  handleResultSelect(result) {
    this.setState({ value: result.title })
  }

  handleSearchChange(value) {
    this.setState({ isLoading: true, value: value })

    setTimeout(() => {
      if (this.state.value.length < 1) return this.setState(initialState)

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.title)
      const filteredData = _.filter(this.state.articles, isMatch)
      this.setState({
        isLoading: false,
        articles: filteredData,
      })
    }, 300)
  }

  handleFilterChange(value) {
    this.setState({ filterValue: value })
    setTimeout(() => {
      if (this.state.filterValue.length === 0) return this.setState(initialState)
      const re = new RegExp(_.escapeRegExp(this.state.filterValue), 'i')
      const isMatch = result => re.test(result.date)
      const filteredData = _.filter(this.state.articles, isMatch)
      this.setState({
        isLoading: false,
        articles: filteredData,
        options: _.uniqBy(filteredData, 'date'),
      })
    }, 300)
  }

  render() {
    const { isLoading, value, articles } = { ...this.state }
    return (
      <Container className="articles">
        <SearchBar
          loading={isLoading}
          onResultSelect={this.handleResultSelect.bind(this)}
          onSearch={this.handleSearchChange.bind(this)}
          onFilter={this.handleFilterChange.bind(this)}
          value={value}
          results={articles}
          options={_.uniqBy(articles, 'date').map((article, index) => {
            return {
              key: index,
              text: article.date,
              value: article.date,
            }
          })}
        />

        <Grid stackable columns={3}>
          {articles.map((data, index) => {
            return (
              <Grid.Column width={5} key={index}>
                <Article data={data} key={index} {...this.props} />
              </Grid.Column>
            )
          })}
        </Grid>
      </Container>
    )
  }
}

export default Articles
