import Home from "../screens/Home";
import Articles from "../screens/Articles";
import Highlights from "../components/Highlights/Highlights";

export const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "/articles",
    component: Articles
  },
  {
    path:"/highlight/:highlight",
    component: Highlights
  }
];
