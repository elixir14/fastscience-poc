import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import NavHeader from './components/NavBars/Header'
import Home from './screens/Home'
import Articles from './screens/Articles'
import Highlights from './components/Highlights/Highlights'

class App extends Component {
  render() {
    return (
      <BrowserRouter >
        <NavHeader />
        <Route exact path="/" render={props => <Home {...props}></Home>}></Route>

        <Route exact path="/articles" render={props => <Articles {...props}></Articles>}></Route>
        <Route excat path="/articles/:article/highlight" component={Highlights}></Route>
      </BrowserRouter>
    )
  }
}

export default App
